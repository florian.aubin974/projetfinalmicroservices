/*
créer une partie (choisir  difficulté, nbr question, catégorie)
appel question + réponse
set score uniquement a la fin (fin de partie route)
*/
const express = require('express')
const axios = require('axios')
const bodyparser = require('body-parser')
const crypto = require('crypto')
const cors = require('cors')

const appExemple = express()
appExemple.use(bodyparser.json(), cors())
const port = 3004;
let monToken;
let questions;
let numeroQuestion = 0;

// Fonction lancée eu démarrage du service
const start = async () => {

    console.log("Gestion parties lancé");
    // console.log(response.data);
    // monToken = response.data.token;

}

async function sendQuestionAndResponse(question, numeroQuestion) {
    let questionUnique = question[numeroQuestion];
    numeroQuestion++;
    console.log(questionUnique);
    return questionUnique;
}

// Création d'un endpoint en GET (ping)
appExemple.get('/ping', async (req, res) => {
    let string = "pong gestion parties";
    console.log(string);
    res.json(string);
})

appExemple.get('/initPartie', async (req, res) => {
    console.log("INIT DE PARTIE")
    let response = await axios.get("http://localhost:3003/getQuestions", {params : {"categorie" : req.query.categorie, "nbrQuestion" : req.query.nbrQuestion, "difficulte" : req.query.difficulte }});
    questions = response.data;
    // await sendQuestionAndResponse(questions, numeroQuestion);
    res.json(questions);
})


appExemple.post('/finDePartie', async (req, res) => {
    console.log("FIN DE PARTIE");
    let response = await axios.post("http://localhost:3006/setScore", {"score" : req.body.score, "userId" : req.body.userId, "categorie" : req.body.categorie});
    res.json(response.data.score);
})

// Lancement du service
appExemple.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
    start();
})

