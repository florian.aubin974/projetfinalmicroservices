/*
mettre en base le score final
renvoyer json des score de l'utilisateur concerné
*/
const express = require('express')
const axios = require('axios')
const bodyparser = require('body-parser')
const crypto = require('crypto')
const fs = require('fs');
const cors = require('cors')
const current = require("./../../bdd/score.json");

const appExemple = express()
appExemple.use(bodyparser.json(), cors())
const port = 3006;
let monToken;

// Fonction lancée eu démarrage du service
const start = async () => {
    console.log("Score lancé");
}

// Création d'un endpoint en GET (ping)
appExemple.get('/ping', async (req, res) => {
    let string = "pong score";
    console.log(string);
    res.json(string);
})

appExemple.get('/getScores', async (req, res) => {
    console.log("get scores")
    let scores = [];
    current.score.forEach(function(table) {
        if (table["id_user"] == req.query.userId){
            scores.push(table);
        }
    });
    res.json(scores);
})


appExemple.post('/setScore', async (req, res) => {
    console.log("REMPLISSAGE DU SCORE");
    let score = {
        id: (Object.keys(current.score).length)+1,
        id_user: req.body.userId,
        scoreNumber: req.body.score,
        date: Date.now(),
        categorie: req.body.categorie
    };
    current.score.push(score);
    fs.writeFileSync('./../../bdd/score.json', JSON.stringify(current, null, 4));
    res.json(score);
})

// Lancement du service
appExemple.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
    start();
})
