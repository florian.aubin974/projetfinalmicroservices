//nouvelleQuestionReponse post body : question , reponse1 reponse2, reponse3, reponse4, bonne réponse (entre 1 et 4)

const express = require('express')
const axios = require('axios')
const bodyparser = require('body-parser')
const crypto = require('crypto')
const currentQuestion = require("./../../bdd/question.json");
const currentReponse = require("./../../bdd/reponse.json");
const fs = require("fs");

const appExemple = express()
appExemple.use(bodyparser.json())
const port = 3007;
let monToken;

// Fonction lancée eu démarrage du service
const start = async () => {

    // Envoie d'une requete
    console.log("Création question");
}

// Création d'un endpoint en GET (ping)
appExemple.get('/ping', async (req, res) => {
    let string = "pong création question";
    console.log(string);
    res.json(string);
})

appExemple.post('/nouvelleQuestionReponse', async (req, res) => {
    console.log("CREATION DE USER");
    let question = {
        id: (Object.keys(currentQuestion.question).length)+1,
        libelle: req.body.libelle.toString(),
        difficulte: req.body.difficulte.toString(),
        categorie: req.body.categorie,
    };

    currentQuestion.question.push(question);
    fs.writeFileSync('./../../bdd/question.json', JSON.stringify(currentQuestion, null, 4));


    let reponse1 = {
        id: (Object.keys(currentReponse.reponse).length)+1,
        id_question: Object.keys(currentQuestion.question).length,
        bonne_reponse: req.body.bonneReponse  == 1,
        libelle: req.body.reponse1,
    };
    currentReponse.reponse.push(reponse1);
    fs.writeFileSync('./../../bdd/reponse.json', JSON.stringify(currentReponse, null, 4));

    let reponse2 = {
        id: (Object.keys(currentReponse.reponse).length)+1,
        id_question: Object.keys(currentQuestion.question).length,
        bonne_reponse: req.body.bonneReponse  == 2,
        libelle: req.body.reponse2,
    };
    currentReponse.reponse.push(reponse2);
    fs.writeFileSync('./../../bdd/reponse.json', JSON.stringify(currentReponse, null, 4));

    let reponse3 = {
        id: (Object.keys(currentReponse.reponse).length)+1,
        id_question: Object.keys(currentQuestion.question).length,
        bonne_reponse: req.body.bonneReponse  == 3,
        libelle: req.body.reponse3,
    };
    currentReponse.reponse.push(reponse3);
    fs.writeFileSync('./../../bdd/reponse.json', JSON.stringify(currentReponse, null, 4));

    let reponse4 = {
        id: (Object.keys(currentReponse.reponse).length)+1,
        id_question: Object.keys(currentQuestion.question).length,
        bonne_reponse: req.body.bonneReponse  == 4,
        libelle: req.body.reponse4,
    };
    currentReponse.reponse.push(reponse4);
    fs.writeFileSync('./../../bdd/reponse.json', JSON.stringify(currentReponse, null, 4));
    res.json(question);
})

// Lancement du service
appExemple.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
    start();
})

