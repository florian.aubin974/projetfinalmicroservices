const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin
module.exports = {
  lintOnSave: false,
  configureWebpack: {
    plugins: [new BundleAnalyzerPlugin()]
  },
  transpileDependencies: ['dom7', 'swiper', 'ss-window', 'vue-awesome-swiper', 'swiper', 'VueGoogleMaps', 'vuex-persist']
}
