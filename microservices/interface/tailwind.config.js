const plugin = require('tailwindcss/plugin')

module.exports = {
  plugins: [
    plugin(function ({ addBase, config }) {
      addBase({
        // html: { fontSize: config('theme.fontSize.base') }
      })
    })
  ],

  future: {
    purgeLayersByDefault: process.env.NODE_ENV === 'production'
  },

  options: {
    prefix: '',
    important: false,
    separator: ':'
  },

  important: false,

  theme: {
    screens: {
      xs: '480px', // Smallest
      phone: '640px', // Phones
      tablet: '768px', // Tablets
      laptop: '1220px', // Laptops
      desktop: '1440px' // Desktops
    },

    container: {
      center: true,
      padding: {
        default: '1rem',
        lg: '0'
      }
    },

    fontFamily: {
      display: ['Roboto', 'sans-serif'],
      body: ['Roboto', 'sans-serif']
    },

    fontSize: {
      xs: '11px',
      sm: '12px',
      base: '13px',
      lg: '16px',
      xl: '18px'
    },

    borderRadius: {
      sm: '6px',
      default: '10px',
      md: '8px',
      lg: '10px',
      xl: '12px',
      full: '9999px'
    },

    extend: {
      fontSize: {
        '2xl': '26px',
        '3xl': '30px'
      },

      inset: {
        4: '1rem',
        8: '2rem'
      },

      colors: {
        primary: {
          default: 'var(--color-primary)',
          hover: 'var(--color-primary-hover)',
          light: 'var(--color-primary-light)',
          lighter: 'var(--color-primary-lighter)'
        },

        secondary: 'var(--color-secondary)',
        tertiary: 'var(--color-tertiary)',

        dark: {
          darker: 'var(--color-dark-darker)',
          default: 'var(--color-dark)',
          lighter: 'var(--color-dark-lighter)'
        },

        gray: {
          100: '#f2f4f7',
          200: '#e9ebf0',
          300: '#e0e0e0',
          400: '#d1d5da',
          500: '#9e9e9e',
          600: '#757575',
          700: '#616161',
          800: '#424242',
          900: '#212121'
        }
      },

      gridTemplateColumns: {
        24: 'repeat(24, minmax(0, 1fr))'
      },

      gridColumn: {
        'span-13': 'span 13 / span 13'
      },

      margin: {
        96: '24rem',
        128: '32rem'
      },

      spacing: {
        '1/1': '100%',
        '1/8': '12.5%'
      }
    }
  },

  variants: {
    opacity: ['responsive', 'hover']
  }
}
