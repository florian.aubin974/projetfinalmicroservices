# frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Frontend integration

Ce projet utilise Tailwind come Framework CSS.
Se référer à la [Documentation de TailwindCSS](https://tailwindcss.com/) pour plus d'informations au sujet du Framework.
