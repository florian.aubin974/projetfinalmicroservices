import Vue from 'vue'
import axios from "axios";
import router from "@/router";
import localStorageService from "@/services/localStorageService"

let $axios = axios.create()

$axios.interceptors.request.use(config => {
  const token = localStorageService.getAccessToken()
  if(token) {
    config.headers['Authorization'] = `Bearer ${token}`
  }
  return config
}, error => {
  return Promise.reject(error);
});

$axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    localStorageService.clearToken()
    router.push('/sign-in');
    return Promise.reject(error);
  }

  return Promise.reject(error)
})

export default $axios;
