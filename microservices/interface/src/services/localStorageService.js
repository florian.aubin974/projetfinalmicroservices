function setAccessToken(token) {
    localStorage.setItem('quizz_access_token', token);
}

function getAccessToken() {
    return localStorage.getItem('quizz_access_token');
}

function getUserId() {
    return localStorage.getItem('quizz_userId');
}

function setUserId(token) {
  localStorage.setItem('quizz_userId', token);
}

function clearToken() {
    localStorage.removeItem('quizz_access_token');
    localStorage.removeItem('quizz_userId');
}

function parseToken(token) {
    if (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    }
}

export default {
    setAccessToken,
    getAccessToken,
    getUserId,
    setUserId,
    clearToken,
    parseToken
}
