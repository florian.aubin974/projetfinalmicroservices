import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import localStorageService from '@/services/localStorageService'

import Login from '@/views/authentication/Login.vue'
import Games from '@/views/pages/games/Games.vue'
import Game from '@/views/pages/games/Question.vue'
import CreateGame from '@/views/pages/games/Form.vue'
import Score from '@/views/pages/games/Score.vue'
import Home from '@/views/pages/home/Home'

Vue.use(VueRouter)

const routes = [
  { path: "/", name: "home", component: Home, meta: { requiresAuth: true } },
  { path: "/games", name: "games", component: Games, meta: { requiresAuth: true }},
  { path: "/create-game", name: "create-game", component: CreateGame, meta: { requiresAuth: true }},
  { path: "/game", name: "game", component: Game, meta: { requiresAuth: true }},
  { path: "/score", name: "score", component: Score, meta: { requiresAuth: true }},
  { path: "/sign-in", name: "sign-in", component: Login },
  { path: "/sign-up", name: "sign-up", component: Login }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const token = localStorageService.getAccessToken()
    if (!token) {
      next({
        name: 'sign-in',
        query: {
          nextUrl: 'add-event'
        }
      })
    }

    /*const parsedJwt = localStorageService.parseToken(token)
    if(Math.floor(Date.now() / 1000) > parsedJwt.exp) {
      localStorageService.clearToken()
      next({
        name: 'sign-in',
        query: {
          nextUrl: 'add-event'
        }
      })
    }*/
  }
  next()
});

export default router
