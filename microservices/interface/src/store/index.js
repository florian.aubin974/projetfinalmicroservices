import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import getters from './getters'
import mutations from './mutations'

import authenticationModule from './modules/authentication'
import gamesModule from './modules/games'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authentication: authenticationModule,
    games: gamesModule
  },

  state: {
  },

  actions,
  mutations,
  getters
})
