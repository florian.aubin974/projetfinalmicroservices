import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const getDefaultState = () => {
  return {
    current: {
      questionsNumber: '1',
      difficulty: '',
      category: '',
      score: 0,
      currentQuestionPosition: 0
    },
    questions: [],
    scores: [],
  }
}

const state = getDefaultState();

mutations.RESET_CLIENTS_STATE = (state) => {
  Object.assign(state, getDefaultState())
}

export default ({
  state: state,
  actions,
  mutations,
  getters
})
