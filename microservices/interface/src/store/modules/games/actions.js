import $axios from '@/axios'
import Vue from "vue";
import localStorageService from "@/services/localStorageService";

const getScores = async ({ commit }) => {
  try {
    const scores = await $axios.get('http://localhost:3006/getScores', { params: { userId: localStorageService.getUserId() } })
    commit('SET_SCORES', scores.data)
  } catch (e) {
    Vue.notify({
      group: 'notification',
      text: 'Une erreur est survenue lors de la récupération des scores',
      type: 'error'
    })
  }
}

const initGame = async ({ state, commit }) => {
  try {
    const res = await $axios.get('http://localhost:3004/initPartie', {
      params: {
        categorie: state.current.category,
        nbrQuestion: state.current.questionsNumber,
        difficulte: state.current.difficulty
      }
    })

    commit('SET_QUESTIONS', res.data.question)

    Vue.notify({
      group: 'notification',
      text: 'La partie a bien été créée',
      type: 'success'
    })
  } catch (e) {
    console.log(e)
    Vue.notify({
      group: 'notification',
      text: 'Une erreur est survenue lors de la création de la partie',
      type: 'error'
    })
  }
}

const setScore = async ({ state }) => {
  try {
    const score = state.current.score + ' / ' + state.current.questionsNumber
    await $axios.post('http://localhost:3004/finDePartie', {
      categorie: state.current.category,
      userId: localStorageService.getUserId(),
      score: score
    })
  } catch (e) {
    console.log(e)
    Vue.notify({
      group: 'notification',
      text: 'Une erreur est survenue lors de l\'envoie du score',
      type: 'error'
    })
  }
}

const resetGamesState = async ({ commit }) => {
  commit('RESET_GAMES_STATE')
}

export default {
  getScores,
  initGame,
  setScore,
  resetGamesState
}
