const SET_SCORES = (state, value) => {
 state.scores = value
}

const SET_GAME = (state, value) => {
  state.current = value
}

const SET_QUESTIONS = (state, value) => {
  state.questions = value
}

const INCREMENT_SCORE = (state) => {
  state.current.score += 1
}

const INCREMENT_QUESTION_POSITION = (state) => {
  state.current.currentQuestionPosition += 1
}

export default {
  SET_SCORES,
  SET_GAME,
  SET_QUESTIONS,
  INCREMENT_SCORE,
  INCREMENT_QUESTION_POSITION
}
