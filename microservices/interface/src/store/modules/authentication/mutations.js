import localStorageService from '@/services/localStorageService'

const SET_USER_ID = (state, id) => {
  localStorageService.setUserId(id)
  state.userId = id
}

const CONNECT_USER = (state, status) => {
  state.isConnected = status
}

const SET_ACCESS_TOKEN = (state, token) => {
  localStorageService.setAccessToken(token)
  state.access_token = token
}

export default {
  SET_USER_ID,
  CONNECT_USER,
  SET_ACCESS_TOKEN
}
