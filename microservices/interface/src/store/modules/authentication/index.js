import actions from './actions'
import getters from './getters'
import mutations from './mutations'

import localStorageService from '@/services/localStorageService'

export default ({
  state: {
    isConnected: !!localStorageService.getAccessToken(),
    access_token: localStorageService.getAccessToken() || undefined,
    userId: localStorageService.getUserId() || undefined,
  },

  actions,
  mutations,
  getters
})
