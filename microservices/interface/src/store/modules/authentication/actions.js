import axios from '@/axios'
import router from '@/router'
import Vue from "vue";
import localStorageService from "@/services/localStorageService";

const requestToken = async ({ commit, dispatch }, payload) => {
  try {
    const auth = await axios.post('http://localhost:3005/getToken', payload)

    commit('SET_ACCESS_TOKEN', auth.data.access_token)
    commit('SET_USER_ID', auth.data.userId)
    commit('CONNECT_USER', true)

    const redirectPath = router.currentRoute.query.redirect || { name: 'games' }
    router.push(redirectPath)

  } catch (err) {
    if(err.response.status === 401) {
      Vue.notify({
        group: 'notification',
        text: 'Identifiants incorrects',
        type: 'error'
      })
    } else {
      Vue.notify({
        group: 'notification',
        text: 'Une erreur est survenue lors de l\'authentification',
        type: 'error'
      })
    }
  }
}

const createUser = async ({ commit, dispatch }, payload) => {
  try {
    await axios.post('http://localhost:3005/createUser', payload)

    Vue.notify({
      group: 'notification',
      text: 'Votre compte a bien été créé',
      type: 'success'
    })

    router.push({ name: 'sign-in' })

  } catch (err) {
    Vue.notify({
      group: 'notification',
      text: 'Une erreur est survenue lors de la création de votre compte',
      type: 'error'
    })
  }
}

const disconnectUser = async ({ commit }) => {
  console.log('[Vuex] disconnectUser action')
  try {
    // Le store vuex est vidé
    commit('CONNECT_USER', false)
    commit('SET_ACCESS_TOKEN', null)
    commit('SET_USER_ID', null)

    localStorageService.clearToken()

    // L'user est renvoyé sur la page de login
    router.push({ name: 'sign-in' })
  } catch (err) {
    console.error(err)
    // Notification de l'erreur
  } finally {
    // Effet quoiqu'il arrive
  }
}

export default {
  requestToken,
  createUser,
  disconnectUser
}
