const isAuthenticated = state => state.isConnected
const refreshToken = state => state.refresh_token
const accessToken = state => state.access_token

export default {
  isAuthenticated,
  refreshToken,
  accessToken
}
