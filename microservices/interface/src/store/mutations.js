const SET_PREVIOUS_ROUTE = (state, name) => {
  state.previousRoute = name
}

export default {
  SET_PREVIOUS_ROUTE
}
