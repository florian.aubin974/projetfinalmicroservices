import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import $axios from './axios'

import Vuelidate from 'vuelidate'
import Notifications from 'vue-notification'

import iconComponent from '@/components/commons/Icon'
import modalComponent from '@/components/commons/Modal'

// Import des styles globaux
import '@/assets/styles/main.scss'

Vue.config.productionTip = false

Vue.prototype.$axios = $axios

// Import des icones custom depuis le dossier d'icone pour un usage global de ce
// composant. Ainsi, pas besoin de l'appeler à chaque fois. Il faut ensuite
// renseigner le nom de l'icone (idem au nom fichier) passée dans les props.
Vue.component('icon', iconComponent)
Vue.component('modal', modalComponent)

Vue.use(Vuelidate)

// https://www.npmjs.com/package/vue-notification
Vue.use(Notifications)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
