//nouveauUtilisateur body : pseudo , password
//createToken body : pseudo , password
//getToken : pseudo, password

/*
créer une partie (choisir  difficulté, nbr question, catégorie)
appel question + réponse
set score uniquement a la fin (fin de partie route)
*/
const express = require('express')
const axios = require('axios')
const bodyparser = require('body-parser')
const crypto = require('crypto')
const current = require("./../../bdd/user.json");
const cors = require('cors')
const fs = require("fs");

const appExemple = express()
appExemple.use(bodyparser.json(), cors())
const port = 3005;
let monToken;

// Fonction lancée eu démarrage du service
const start = async () => {

    // Envoie d'une requete
    console.log("Authentification");
}

// Création d'un endpoint en GET (ping)
appExemple.get('/ping', async (req, res) => {
    let string = "pong authentification";
    console.log(string);
    res.json(string);
})

appExemple.get('/verifToken', async (req, res) => {
    console.log("VERIF TOKEN")
    let ret = false;
    current.user.forEach(function(table) {
        if (table["pseudo"] == req.query.pseudo){
            ret = true;
            res.json(ret);
        }
    });
    res.json(ret);
})

appExemple.post('/getToken', async (req, res) => {
    console.log("GET TOKEN")
    let ret = false;
    current.user.forEach(function(table) {
        if (table["pseudo"] == req.body.pseudo
        && table["password"] == req.body.password){
            ret = true;
            res.json({ access_token: table.pseudo, userId: table.id });
        }
    });
    res.status(401).end();
})


appExemple.post('/createUser', async (req, res) => {
    console.log("CREATION DE USER");
    let user = {
        id: (Object.keys(current.user).length)+1,
        pseudo: req.body.pseudo,
        password: req.body.password,
    };
    current.user.push(user);
    fs.writeFileSync('./../../bdd/user.json', JSON.stringify(current, null, 4));
    res.json(user);
})

// Lancement du service
appExemple.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
    start();
})

