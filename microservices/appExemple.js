
const express = require('express')
const axios = require('axios')
const bodyparser = require('body-parser')
const crypto = require('crypto')

const appExemple = express()
appExemple.use(bodyparser.json())
const port = 3000;
let secret_key;
let public_key;
let monToken;

// Fonction lancée eu démarrage du service
const start = async () => {

    // Envoie d'une requete
    let response;
    let responseEncrypt
    try {
        response = await axios.post("http://localhost:1338/register", {"host" : 'http://10.44.16.191:3000' , 'code' : 'florian.aubin'}, {auth : { 'username' : 'ynovset', 'password' : 'tHuds4752_525@'}});

    }catch(e){
        console.error(e.response ? e.response.data : e)
        return
    }
    console.log(response.data);
    monToken = response.data.token;
    public_key = response.data.public_key;
    secret_key = response.data.secret_key;

    try{
        responseEncrypt = await axios.get("http://localhost:1338/registry", {headers : { "x-auth-token": monToken }});
    } catch (e){
        console.error(e.response ? e.response.data : e)
        return

    }

    try{
        responseEncrypt.data.forEach(element => {
            getKeyes(element.host, element.code)
        })
    } catch (e){
        console.error(e.response ? e.response.data : e)
        return
    }

}

async function getKeyes(host, code) {
    console.log("----- GETKEYES ----");
    console.log(monToken);
    let responsegetKey = await axios.get(host+"/getkey", {headers : { "x-auth-token": monToken }});
    console.log(responsegetKey.data);

    let response = await axios.post("http://localhost:1338/key/unlock", {"code" : code , 'key' : responsegetKey.data["encrypted_public_key"]}, {headers : { "x-auth-token": monToken }});
    console.log("Unlock");
    console.log(response.data);
}

// Création d'un endpoint en GET (ping)
appExemple.get('/ping', async (req, res) => {
    // Récuperer les headers
    let headers = req.headers;
    res.json()
})

appExemple.get('/getkey', async (req, res) => {
    // Récuperer les headers
    let headers = req.headers;
    let tokenAVerifier = headers["x-auth-token"];
    let response = await axios.post("http://localhost:1338/token/validate", {"token" : tokenAVerifier}, {headers : { "x-auth-token": monToken }});
    console.log(response.data.valid);
    if (response.data.valid){
        let resKey = encrypt(secret_key, public_key);
        res.json({'encrypted_public_key' : resKey});
    } else {
        res.status(403).end();
    }
})


appExemple.post('/newservice', async (req, res) => {
    console.log("---------------- NEW SERVICE ---------------")
    try{
        let element = req.body;
        console.log(element);
            await getKeyes(element.host, element.code)
    } catch (e){
        console.error(e.response ? e.response.data : e)
        return
    }
})

// Création d'un endpoint en GET
appExemple.get('/url_du_endpoint_en_get', async (req, res) => {
    // Récuperer les headers
    let headers = req.headers;
    res.json()
})

// Création d'un endpoint en POST
appExemple.post('/url_du_endpoint_en_post', async (req, res) => {
    // Récuération du body
    let body = req.body;
    let headers = req.headers;
    console.log(body)
    console.log(headers)
    res.json()
})


// Lancement du service
appExemple.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
    start();
})



/**
 * Fonction de chiffrement
 * @param secretKey
 * @param publicKey
 * @returns {string}
 */
function encrypt(secretKey , publicKey){
    return crypto.createHmac('sha256', secretKey)
        .update(publicKey)
        .digest('hex');
}
