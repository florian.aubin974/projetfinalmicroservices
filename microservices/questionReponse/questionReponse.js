
const express = require('express')
const bodyparser = require('body-parser')
let FakeQuestionBDD = require('../../bdd/question.json')
let FakeResponceBDD = require('../../bdd/reponse.json')

const app = express()
app.use(bodyparser.json())
const port = 3003;
// swagger configuration
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'API REST Good Books',
        version: '1.0.0',
        description: 'This is REST API for Good Book library'
    },
    components: {
        securitySchemes: {
            jwt: {
                type: 'http',
                name: 'Authorization',
                scheme: 'bearer',
                in: 'header',
                bearerFormat: 'JWT',
            }
        }
    },

    security: [{
        jwt: []
    }],

    servers: [
        {
            url: `http://localhost:${port}`,
            description: 'Local server'
        }
    ]
};

const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['questionReponse.js'],
};

const swaggerSpec = swaggerJSDoc(options);
const swaggerUi = require('swagger-ui-express');
const { json, response } = require('express')

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
})

// Lancement du service
app.listen(port, () => {
    console.log(`Service listening at http://localhost:${port}`)
})



//---------------------------Routes------------------------

/**
 * @openapi
 * /allquestion:
 *   get:
 *     summary: Retrieve a list of question
 */
app.get('/allquestion', async (req, res) => {
    // Récuperer les headers
    let headers = req.headers;
    res.json(FakeQuestionBDD)
})

/**
 * @openapi
 * /allquestion:
 *   get:
 *     summary: Retrieve a list of reponse
 */
 app.get('/allreponse', async (req, res) => {
    // Récuperer les headers
    let headers = req.headers;
    res.json(FakeQuestionBDD)
})


/**
 * @openapi
 * /getQuestions:
 *   get:
 *     summary: Retrieve a list of question and reponse
 */
app.get('/getQuestions', async (req, res) => {
    // Récuperer les headers
    let categorie = req.query.categorie;
    let nbrQuestion = req.query.nbrQuestion;
    let difficulte = req.query.difficulte;
    let cpt = 0;
    let cptTableau=0;
    let Jsonreponse = {
        question: []
    };

    while(cptTableau <= (nbrQuestion - 1))
    {
        if(FakeQuestionBDD.question[cpt].categorie == categorie &&
            FakeQuestionBDD.question[cpt].difficulte == difficulte){
                Jsonreponse.question[cptTableau] = FakeQuestionBDD.question[cpt]
                Jsonreponse.question[cptTableau].reponse = []
                FakeResponceBDD.reponse.forEach(element => {
                    if(element.id_question == FakeQuestionBDD.question[cpt].id)
                    {
                    Jsonreponse.question[cptTableau].reponse.push(element)
                    }
                });
                cptTableau++;
        }
        cpt++;
    }

    res.json(Jsonreponse)
})
