# ProjetFinalMicroservices



## Getting started

```
npm install --prefix .\microservices\authentification\

npm install --prefix .\microservices\creationQuestion\

npm install --prefix .\microservices\gestionParties\

npm install --prefix .\microservices\questionReponse\

npm install --prefix .\microservices\score\

npm install --prefix .\microservices\interface\

node .\microservices\authentification\authentification.js

node .\microservices\creationQuestion\creationQuestion.js

node .\microservices\gestionParties\gestionParties.js

node .\microservices\questionReponse\questionReponse.js

node .\microservices\score\score.js

npm run serve --prefix .\microservices\interface\
```

Aller sur localhost:8080

